from PIL import Image
from tesserocr import PyTessBaseAPI, RIL

image = Image.open('screenshot.png')

with PyTessBaseAPI() as api:
	api.SetImage(image)

	boxes = api.GetComponentImages(RIL.TEXTLINE, True)
	print 'Found {} textline image components.'.format(len(boxes))

	for i, (im, box, _, _) in enumerate(boxes):
		api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
		ocrResult = api.GetUTF8Text()

		print ocrResult

		conf = api.MeanTextConf()

		print conf

		result = (u"Box[{0}]: x={x}, y={y}, w={w}, h={h}, "
			"confidence: {1}, text: {2}").format(i, conf, ocrResult, **box)

		#result2 = (u"text: {0}").format(ocrResult, **box)

		print result
